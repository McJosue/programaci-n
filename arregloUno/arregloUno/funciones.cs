﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejerciciosClase
{
    class funciones
    {
        public static void numDecimales()
        {
            decimal a, b;
            Console.WriteLine("Escribe un numero decimal");
            a = decimal.Parse(Console.ReadLine());
            do
            {
                Console.WriteLine("Escribe otro numero");
                b = decimal.Parse(Console.ReadLine());
            }
            while (b > a);
            Console.WriteLine("salir");
        }
        public static void tablaMult()
        {
            int a, tab;
            Console.WriteLine("Elije una tabla de multiplicar del 1 al 10");
            a = int.Parse(Console.ReadLine());
            for (tab = a; tab <= a; tab++)
            {
                for (tab = 1; tab <= 10; tab++)
                {
                    Console.WriteLine("por " + tab + " * " + a + " es: " + tab * a);
                }
            }
        }
        public static void codigoContraseña()
        {
            string codigob, contra;
            string codigoa = "1024";
            string contrab = "4567";

            do
            {
                Console.WriteLine("Escribe el código");
                codigob = Console.ReadLine();
                if (codigoa != codigob)
                {
                    Console.WriteLine("Código incorrecto \n");
                }
            }
            while (codigoa != codigob);
            Console.WriteLine("Código correcto \n");
            do
            {
                Console.WriteLine("Escribe la contraseña");
                contra = Console.ReadLine();
                if (contrab != contra)
                {
                    Console.WriteLine("Contraseña incorrecta \n");
                }
            }
            while (contrab != contra);
            Console.WriteLine("Contraseña correcta \n");
        }
    }
}
