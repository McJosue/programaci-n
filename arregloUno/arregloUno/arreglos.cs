﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejerciciosClase
{
    class arreglos
    {
        public static void numerosAscendentes()
        {
            Console.WriteLine("Los numeros del 1 al 100 son:");
            int[] arreglo = new int[1];
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(i + 1);
            }

        }

        public static void numerosDescendentes()
        {
            Console.WriteLine("Los numeros del 100 al 1");
            int[] arreglo = new int[1];
            for (int i = 100; i > 0; i--)
            {
                Console.WriteLine(i);
            }
        }
        public static void operaciones()
        {
            Console.WriteLine("Ingrese 10 numeros \n");
            double suma = 0, resta = 0, prod = 1, div=1,b;
            double[] arreglo = new double[10];
            for (int i = 0; i < 10; i++)
            {

                b = double.Parse(Console.ReadLine());
                suma = suma + b;
                resta = resta - b;
                prod = prod * b;
                div = div/b;
            }
            Console.WriteLine("La suma es "+suma);
            Console.WriteLine("La resta es " + resta);
            Console.WriteLine("El producto es " + prod);
            Console.WriteLine("El cociente es " + div);
        }

    }
}
