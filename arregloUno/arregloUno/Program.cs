﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejerciciosClase
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Elije una opcion: \n \n" +
                "1. Numeros decimales mayores \n" +
                "2. Tabla de multiplicar \n" +
                "3. Codigo y contraseña \n" +
                "4. 100 primeros numeros en orden ascendente \n" +
                "5. 100 primeros numeros en orden descendente \n" +
                "6. Suma, resta, multiplicacion y división \n");
            a = int.Parse(Console.ReadLine());
                switch (a)
                {
                case 1:
                    funciones.numDecimales();
                    break;
                case 2:
                    funciones.tablaMult();
                    break;
                case 3:
                    funciones.codigoContraseña();
                    break;
                case 4:
                        arreglos.numerosAscendentes();
                        break;
                    case 5:
                        arreglos.numerosDescendentes();
                        break;
                    case 6:
                        arreglos.operaciones();
                        break;
                }
            Console.WriteLine("saldra del programa");
            Console.ReadKey();
        }
    }
}
