﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Ingrese los minutos");
            a = int.Parse(Console.ReadLine());
            if (a < 60)
            {
                Console.WriteLine("Se le cobrara 5.50 pesos");
            }
            else if (a == 60)
            {
                Console.WriteLine("Se le cobrara 6.00 pesos");
            }
            else if (a > 60)
            {
                double b;
                b = ((a - 60) * 0.15) + 6;
                Console.WriteLine("Se le cobrara " + b);
            }
            Console.ReadKey();
        }
    }
}
