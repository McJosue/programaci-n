﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Ingrese el sueldo");
            a = int.Parse(Console.ReadLine());
            if (a>3000)
            {
                Console.WriteLine("Debe pagar un impuesto de 16%");
            }
            else
            {
                Console.WriteLine("No paga impuesto");
            }
            Console.ReadKey();
        }
    }
}
