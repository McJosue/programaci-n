﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicioUno
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, d;
            Console.WriteLine("operaciones de dos numeros");
            Console.WriteLine("Ingrese el primer valor");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el segundo valor");
            b = int.Parse(Console.ReadLine());
            if (a > b)
            {
                c = a + b;
                d = a - b;
                Console.WriteLine("La suma es " +c);
                Console.WriteLine("La diferencia es " + d);
            }
            else if (b>a)
            {
                c = a * b;
                d = a / b;
                Console.WriteLine("El producto es " + c);
                if (b!=0)
                {
                    Console.WriteLine("El cociente es " + d);
                }
                else
                    Console.WriteLine("Valor no valido");
            }
            Console.ReadKey();
        }
    }
}
