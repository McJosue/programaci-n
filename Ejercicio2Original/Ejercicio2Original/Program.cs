﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2Original
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Introduce un numero del 1 al 99");
            a = int.Parse(Console.ReadLine());
            if (a < 10)
            {
                Console.WriteLine("El numero solo tiene un digito");
            }
            else
            {
                Console.WriteLine("El numero tiene dos digitos");
            }
            Console.ReadKey();
        }
    }
}
