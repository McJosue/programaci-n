﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace multiplicar
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, tab;
            Console.WriteLine("Elije una tabla de multiplicar del 1 al 10");
            a = int.Parse(Console.ReadLine());
            for (tab = a; tab <= a; tab++)
            {
                for (tab = 1; tab <= 10; tab++)
                {
                    Console.WriteLine("por " + tab + " * " + a + " es: " + tab * a);
                }
            }
            Console.ReadKey();
        }
    }
}