﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practicaEstructurasRepetitivas
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("elije un opcion \n" +
                "1. Suma del 1 al 100 \n" +
                "2. Aprobados en informatica \n" +
                "3. Trabajadores mayores de 65 \n" +
                "4. Salarios\n" +
                "5. valores de medias altas");
            a = int.Parse(Console.ReadLine());
                switch (a)
                {
                    case 1:
                        Console.WriteLine("La suma del 1 al 100 con for es " + funciones.suma());
                        Console.WriteLine("La suma del 1 al  con while es " + funciones.sumaWhile());
                        Console.WriteLine("La suma del 1 al  con do while es " + funciones.sumaDoWhile());
                    break;
                    case 2:
                        int b;
                        Console.WriteLine("Escribe la cantidad de alumnos");
                        b = int.Parse(Console.ReadLine());
                        Console.WriteLine("La cantidad de aprobados es " + funciones.calificaciones(b));
                        break;
                    case 3:
                        int t;
                        Console.WriteLine("Escribe la cantidad de trabajadores");
                        t = int.Parse(Console.ReadLine());
                        Console.WriteLine("Existen trabajadores mayores de 65 años en un número de " + funciones.trabajadores(t));
                        break;
                    case 4:
                    funciones.salarios();
                        break;
                    case 5:
                        int x,y;
                    Console.WriteLine("Escribe un numero");
                    x = int.Parse(Console.ReadLine());
                    y = x + 1;
                    Console.WriteLine("su sucesivo es " + y);
                    funciones.media(x,y);
                    break;

                    default:
                        Console.WriteLine("Opcion no valida");
                        break;
                }
            Console.ReadKey();
        }
    }
}
