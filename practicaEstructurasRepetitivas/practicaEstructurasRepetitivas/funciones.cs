﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practicaEstructurasRepetitivas
{
    class funciones
    {
        public static int suma()
        {
            int suma = 0;
          for (int i=1; i<101; i++)
            {
                suma = suma + i;
            }
            return suma;
        } 
        public static int sumaWhile()
        {
            int sumawhile = 0, a=0;
            while (a < 100)
            {
                a = a + 1;
                sumawhile = sumawhile + a;
            }
            return sumawhile;
        }
        public static int sumaDoWhile()
        {
            int sumadowhile = 0, a=0;
            do
            {
                a = a + 1;
                sumadowhile = sumadowhile + a;
            }
            while (a<100);
            return sumadowhile;

        }
        public static int calificaciones(int b)
        {
            int suma=0,c;
            Console.WriteLine("\n Escribe las calificaciones");
            for (int i=0; i< b; i++)
            {
                c= int.Parse(Console.ReadLine());
                if (c >= 5)
                {
                    suma = suma + 1;
                }

            }
            return suma;
        }
        public static int trabajadores(int t)
        {
            int edad, may = 0;
            Console.WriteLine("\n Escribe sus edades");
            for (int i=0; i<t;i++)
            {
                edad = int.Parse(Console.ReadLine());
                if (edad > 65)
                {
                    may = may + 1;
                }
            }
            return may;
        }
        public static void salarios()
        {
            int altos = 0, medios = 0, bajos = 0;
            Console.WriteLine("Escribe los salarios de los 50 trabajadores");
            for (int i = 0; i < 50; i++)
            {
                int salario = int.Parse(Console.ReadLine());
                if (salario > 300000)
                {
                    altos = altos + 1;
                }
                if (salario < 300000 && salario > 100000)
                {
                    medios = medios + 1;
                }
                if (salario < 100000)
                {
                    bajos = bajos + 1;
                }
            }
            Console.WriteLine("Empleados con salarios altos son: " + altos);
            Console.WriteLine("Empleados con salarios medios son: " + medios);
            Console.WriteLine("Empleados con salarios bajos son: " + bajos);
        }
        public static void media(int x,int y)
        {
            double m1, m2;
            m1 = x + y / 2;
            Console.WriteLine("escribe otro numero");
                int w = int.Parse(Console.ReadLine());
                int z = w + 1;
            Console.WriteLine("su sucesivo es "+y);
                m2 = w + z / 2;
            do
            {
                if (m1 < m2)
                {
                    Console.WriteLine("valor maximo de las medias: " + m2);
                }
                else
                {
                    Console.WriteLine("valor maximo de las medias: " + m1);
                }
                break;
            }
            while (m1 > m2);
        }
    }
}
