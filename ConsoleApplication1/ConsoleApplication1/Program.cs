﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            funciones.bienvenida();
            int t;
            t = int.Parse(Console.ReadLine());
            switch (t)
            {
                case 1:
                    funciones.gerentes();
                    break;
                case 2:
                    funciones.porHora();
                    break;
                case 3:
                    funciones.porComision();
                    break;
                case 4:
                    funciones.porDestajo();
                    break;
            }
            Console.ReadKey();
        }
    }
}
