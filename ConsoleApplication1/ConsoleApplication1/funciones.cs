﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class funciones
    {
        public static void bienvenida()
        {
            Console.WriteLine("Hola, vamos a calcular el salario semanal de los empleados \n" +
                "¿Que tipo de trabajador es? \n \n" +
                "1. gerentes/administradores \n" +
                "2. empleados por hora \n" +
                "3. empleados por comision \n" +
                "4. empleados por destajo \n");
        }
        public static void gerentes()
        {
            int a, s;
            Console.WriteLine("Escribe el numero de trabajadores");
            a = int.Parse(Console.ReadLine());
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("\n Escribe el salario semanal del trabajador " + (i + 1));
                s = int.Parse(Console.ReadLine());
                Console.WriteLine("\n Salario semanal=" + s);
            }
        }
        public static void porHora()
        {
            int a, horas, dia, pago, sem;
            double horasextras, semE, semana;
            Console.WriteLine("\n Escribe el numero de trabajadores");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("\n Escribe el pago por hora de los trabajadores");
            pago = int.Parse(Console.ReadLine());
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("\n Escribe las horas trabajadas por dia4 del empleado " + (i + 1));
                horas = int.Parse(Console.ReadLine());
                if (horas <= 8)
                {
                    dia = horas * pago;
                    sem = dia * 7;
                    Console.WriteLine("\n Su pago semanal es " + sem);
                }
                else
                {
                    dia = 8 * pago;
                    sem = dia * 7;
                    horasextras = (horas - 8) * (pago * 1.5);
                    semE = horasextras * 7;
                    semana = sem + semE;
                    Console.WriteLine("\n Su pago semanal es " + semana);
                }
            }
        }
        public static void porComision()
        {
            int a;
            double ventas, comision, sem;
            Console.WriteLine("Escribe el numero de trabajadores");
            a = int.Parse(Console.ReadLine());
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("\n Escribe el monto de ventas realizadas por el trabajador " + (i + 1));
                ventas = int.Parse(Console.ReadLine());
                comision = ventas * 0.057;
                sem = (250 * 7) + comision;
                Console.WriteLine("\n Su pago semanal es " + sem);
            }
        }
        public static void porDestajo()
        {
            int a, p;
            double pieza, semana;
            Console.WriteLine("Escribe el numero de trabajadores");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("\n Escribe el monto por pieza");
            pieza = int.Parse(Console.ReadLine());
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("\n Escribe la cantidad de piezas en la semana del trabjador "+(i+1));
                p = int.Parse(Console.ReadLine());
                semana = p * pieza;
                Console.WriteLine("Su pago semanal es " + semana);
            }
        }
    }
}