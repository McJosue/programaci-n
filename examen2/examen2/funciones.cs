﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen2
{
    class funciones
    {
        public static void bienvenida()
        {
            Console.WriteLine("Hola, debido a que los limites de credito se reduciran \n"+
                "vamos a calcular el nuevo limite de credito de cada cliente \n");
        }
        public static double nuevoLimite(double a)
        {
            double nuevolim;
            nuevolim=a/2;
            return nuevolim;
        }
        public static string limiteExcedido(double a,double b)
        {
            if (a < b)
            {
                return "Se excede del limite";
            }
            else
            {
                return "No se excede del limite";
            }
        }
        public static string creditoExcedido(double a)
        {
            if (10000 < a)
            {
                return "Alerta: El limite de credito excede el credito maximo";
            }
            else
            {
                return "";
            }
        }
        public static int clientesQueExceden(int a, double b)
        {
            int suma =0;
            for (int i = 0; i < a; i++)
            {
                if (10000 < b)
                {
                    suma = suma + 1;
                }
            }
            return suma;
        }
        public static double perdida(int a, double b)
        {
            double perdidas = 0;
            for (int i = 0; i < a; i++)
            {
                if (10000 < b)
                {
                    perdidas = perdidas + b;
                }
            }
            return perdidas;
        }
    }
}
