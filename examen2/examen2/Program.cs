﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen2
{
    class Program
    {
        static void Main(string[] args)
        {

            funciones.bienvenida();
            int c;
            Console.WriteLine("Escribe el numero de clientes");
            c = int.Parse(Console.ReadLine());

            int cuenta;
            double saldo, lim, limite = 0;
            for (int i = 0; i < c; i++)
            {
                Console.WriteLine("\n Debe ingresar el numero de cuenta del cliente " + (i + 1));
                cuenta = int.Parse(Console.ReadLine());
                Console.WriteLine("\n Ahora ingresa el limite de credito del cliente antes de la recesion " + (i + 1));
                lim = double.Parse(Console.ReadLine());
                Console.WriteLine("\n Ahora ingresa el saldo actual del cliente " + (i + 1));
                saldo = double.Parse(Console.ReadLine());

                Console.WriteLine("\nEl nuevo limite de credito es: " + funciones.nuevoLimite(lim));
                Console.WriteLine(funciones.limiteExcedido(funciones.nuevoLimite(lim), saldo));
                Console.WriteLine(funciones.creditoExcedido(funciones.nuevoLimite(lim)));
                limite = funciones.nuevoLimite(lim);
            }
            Console.WriteLine("La cantidad de clientes que exceden el credito son: " + funciones.clientesQueExceden(c, limite));
            Console.WriteLine("La cantidad perdida del credito es: " + funciones.perdida(c,limite));
            Console.ReadKey();
        }
    }
}